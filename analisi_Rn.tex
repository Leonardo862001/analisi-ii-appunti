\section{Analisi in \R^n}
\subsection{Differenziabilità e continuità}
\begin{definition}[Derivata parziale]
	Sia $f : A \subset \R^n \to \R$ una funzione definita su un sottoinsieme aperto dello spazio $\R^n$\\
	sia $\{\vec{e}_i\}_{1 \leq i \leq n}$ la base canonica di $\R^n$\\
	Si definisce la derivata parziale di $f$ rispetto ad $x_i$ come
	\[
		\pdv{f}{x_i} = \lim_{h\to 0} \frac{f(\vec{x}+h\vec{e}_i)-f(\vec{x})}{h} = \frac{f(x_1,x_2,\ldots,x_i+h,\ldots, x_n)-f(x_1,x_2,\ldots,x_n)}{h}
	\]
\end{definition}
\begin{definition}[Derivata parziale in \R^2]
	Si consideri una funzione $f$ con dominio in $\mathbb{R}^{2}$, insieme formato da tutte le coppie ordinate $(x, y)$ con $x, y \in \mathbb{R}$, e con valori in $\mathbb{R}$. Tale funzione in ogni punto $\left(x_{0}, y_{0}\right)$ del proprio dominio può essere derivata sia rispetto a $x$ :
	$$
		f_{x}\left(x_{0}, y_{0}\right)=\frac{\partial f}{\partial x}\left(x_{0}, y_{0}\right)=\lim _{h \rightarrow 0} \frac{f\left(x_{0}+h, y_{0}\right)-f\left(x_{0}, y_{0}\right)}{h}
	$$
	sia rispetto a $y$ :
	$$
		f_{y}\left(x_{0}, y_{0}\right)=\frac{\partial f}{\partial y}\left(x_{0}, y_{0}\right)=\lim _{k \rightarrow 0} \frac{f\left(x_{0}, y_{0}+k\right)-f\left(x_{0}, y_{0}\right)}{k}
	$$
	Se entrambi i limiti esistono finiti, allora la funzione $f$ si dice derivabile in $(x_0,y_0) \in \R^2$
\end{definition}
\begin{definition}[Gradiente]
	si definisce il gradiente di $f$ il vettore che ha come componenti le derivate parziali della funzione $f$
	\[
		\mathrm{grad} f = \grad f = (f_{x_1},f_{x_2},\ldots,f_{x_n})
	\]
\end{definition}
\begin{definition}[Norma euclidea]
	\[
		\|\vec{x}\| = \sqrt{x_1^2+x_2^2+\ldots+x_n^2}
	\]
\end{definition}
\begin{definition}[Intorno aperto]
	intorno di centro $\vec{x}_0$ e raggio $\delta$
	\[
		\left\{x \in \R^n : \|\vec{x}-\vec{x}_0\| < \delta \right\}
	\]
	Sia  $A$ un insieme aperto dello spazio euclideo, $\forall \vec{x} \in A, \exists \delta > 0$ tale che $I(\vec{x}, \delta) \subset A$
\end{definition}
\begin{definition}[Limite]
	una funzione $f:X\subseteq \R^n \to \R$ ha limite $\ell$ in un punto di accumulazione $\vec{x}_0$ per $X$ se
	\[
		\forall \varepsilon > 0, \exists \delta > 0 \text{ tale che } |f(\vec{x}) - \ell | < \varepsilon, \forall \vec{x} \in X \text{ con } 0<\|\vec{x}-\vec{x}_0\| < \delta
	\]
	\[
		\lim_{\vec{x} \to \vec{x}_0} f(\vec{x}) = \ell
	\]
\end{definition}
\begin{theorem}[Unicità del limite]
	Se esiste il limite $\ell$, il limite è unico.
\end{theorem}

Per mostrare che un limite non esiste è sufficiente mostrare due direzioni per cui la funzione tende a due valori diversi\\
Se da due o più direzioni il limite tende ad uno stesso valore non è possibile stabilire che quello sia il limite.

\begin{definition}[Continuità]
	Sia $f: X \to  \R$ e $\vec{x}_0 \in X$ di accumulazione per $X$\\
	Le due proprietà sono equivalenti\\
	\begin{enumerate}
		\item $\forall \varepsilon > 0, \exists \delta > 0$ tale che se $\vec{x} \in X$ e $\|\vec{x}-\vec{x}_0\| < \delta$, allora
		      \[
			      |f(\vec{x}) - f(\vec{x}_0 )| < \varepsilon
		      \]
		\item $\{x_n\}_{\N}$ $x_n \in X$ e $x_n \to x_0$, allora $f(x_n) \to f(x_0)$
	\end{enumerate}
\end{definition}
\begin{definition}[Continuità in \R^2]
	Sia $X \subset \mathbb{R}^{2},\left(x_{0}, y_{0}\right) \in X$ punto di accumulazione per $\mathrm{X}$. $f$ continua in $\left(x_{0}, y_{0}\right)$ se
	$$
		\lim _{(x, y) \rightarrow\left(x_{0}, y_{0}\right)} f(x, y)=f\left(x_{0}, y_{0}\right)
	$$
	$\forall \epsilon>0 \exists \delta>0: \forall(x, y) \in X$ tale che $\sqrt{\left(x-x_{0}\right)^{2}+\left(y-y_{0}\right)^{2}}<\delta$ risulta
	$$
		\left|f(x, y)-f\left(x_{0}, y_{0}\right)\right|<\epsilon
	$$
\end{definition}

\begin{proposition}
	La derivabilità in $\R^n$ non implica la continuità
\end{proposition}

\begin{theorem}[Teorema di inversione di Schwarz]
	\label{schwarz}
	Sia $f: A \subset \R^2 \to \R$ una funzione in due variabili, definita su un aperto $A$ di $\R^2$, se $f$ ammette derivate seconde miste continue, cioè $f \in C^2(A)$ allora
	\[
		\frac{\partial^2 f}{\partial x \partial y} = \frac{\partial^2 f}{\partial y \partial x}
	\]
\end{theorem}
\begin{proof}
	sia $p=(x_0,y_0) \in A$ si scelgono due reali $\varepsilon, \delta > 0$ tali che $(x_0-\varepsilon,x_0 +\varepsilon) \cross (y_0-\delta,y_0+\delta) \subset A$ ciò è possibile perché $A$ è un aperto in $\R^2$\\
	Si definiscono le funzioni $F$ e $G$
	\[
		\begin{gathered}
			F:(-\varepsilon,\varepsilon) \subset \R \to \R \\
			G:(-\delta,\delta) \subset \R \to \R
		\end{gathered}
	\]
	In modo che
	\[
		\begin{gathered}
			F(t) = f(x_0+t,y_0+s)-f(x_0+t,y_0) \qquad \forall s \in (-\delta, \delta)\\
			G(s) = f(x_0+t,y_0+s)-f(x_0,y_0+s) \qquad \forall t \in (-\varepsilon, \varepsilon)\\
		\end{gathered}
	\]
	Fissati $s$ e $t$ si prova facilmente che
	\[
		F(t)-F(0) = G(s)-G(0)
	\]
	Applicando due volte il teorema di Lagrange
	\[
		F(t)-F(0)=tF^{\prime}(\xi_1)=t\left[f_x (x_0+\xi_1,y_0+s)-f_x(x_0+\xi_1,y_0)\right]=ts[f_{xy}(x_0+\xi_1,y_0+\sigma_1)]
	\]
	Analogamente
	\[
		G(s)-G(0) = st[f_{yx}(x_0+\xi_2,y_0+\sigma_2)]
	\]
	\[
		\xi_i \in (0,t) \quad \sigma_i \in (0,s)
	\]
	facendo tendere $s$ e $t$ a $0$, tendono a $0$ anche $\xi_i$ e $\sigma_i$, per la continuità delle derivate miste seconde si ha
	\[
		\pdv{f}{x}{y}(x_0,y_0) = \pdv{f}{y}{x}(x_0,y_0)
	\]
\end{proof}

\begin{definition}[Matrice Hessiana]
	$f \in C^2(\Omega)$
	\[
		Hf(\vec{x})=(f_{x_i,x_j}(\vec{x}))_{i,j=1,\ldots,n}
	\]
\end{definition}
\begin{definition}[Differenziabilità]
	Una funzione $f:A \subset \R^n \to  \R$ definita su un insieme aperto di $\R^n$ è detta differenziabile in un punto $\vec{x}_0$ del dominio se esiste un $\vec{p} \in \R^n$ tale che
	\[
		\lim_{\vec{h}\to 0} \frac{f(\vec{x}+\vec{h})-f(\vec{x})-\vec{p}\cdot \vec{h}}{\|\vec{h}\|}=0
	\]
	$\vec{p} = \grad f(\vec{x})$ infatti $\vec{h} = t \vec{e}_i = (0,\ldots,0,t,0,\ldots,0)$
	\[
		\lim_{t\to 0} \frac{f(\vec{x}+t\vec{e}_i)-f(\vec{x})-tp_i}{|t|} = 0
	\]
	Abbiamo
	\[
		\begin{gathered}
			\lim_{t \to 0} \frac{f(\vec{x}+t\vec{e}_i)-f(\vec{x})-tp_i}{t}=0\\
			\lim_{t \to 0} \frac{f(\vec{x}+t\vec{e}_i)-f(\vec{x})}{t}=p_i\\
		\end{gathered}
	\]
	Quindi $f$ ammette derivate parziali e $p_i=f_{x_i}$\\


	Caso $n=2$
	\[
		\frac{f(x,y)-f(x_0,y_0)-f_x(x_0,y_0)(x-x_0)-f_y(x_0,y_0)(y-y_0)}{\sqrt{(x-x_0)^2+(y-y_0)^2} } \to 0
	\]
	per
	\[
		\sqrt{(x-x_0)^2+(y-y_0)^2}\to 0
	\]
	\[
		f(x,y)-f(x_0.y_0)=\frac{\partial f(x_0,y_0)}{\partial x} (x_0,y_0)+ \frac{\partial f(x_0,y_0)}{\partial y}(x_0,y_0)+o(\sqrt{(x-x_0)^2+(y-y_0)^2})
	\]
	Una funzione differenziabile in un punto risulta approssimabile a meno di un resto infinitesimo da una funzione lineare in un intorno abbastanza piccolo di quel punto
\end{definition}
\begin{proposition}
	la differenziabilità implica la continuità
\end{proposition}
\begin{theorem}[Teorema del differenziale]
Sia $f$ dotata di derivate parziali prime  in un aperto $A$ di $\R^n$, se le derivate parziali prime sono continue in $A$ allora $f$ è differenziabile in $A$\\
La condizione è sufficiente ma non necessaria
\end{definition}
\begin{proof}
	Caso $n=2$
	dobbiamo dimostrare
	\[
		f(x+h,y+k)-f(x,y)-f_x(x,y)h -f_y(x,y)k = o(\sqrt{h^2+k^2})
	\]
	per
	\[
		(h,k) \to (0,0)
	\]
	consideriamo
	\[
		f(x+h,y+k)+f(x,y+k)-f(x,y+k)-f(x,y)-f_x(x,y)h -f_y(x,y)k = o(\sqrt{h^2+k^2})
	\]
	Applichiamo il teorema di Lagrange ad entrambe le derivate prime
	\[
		f(x+h,y+k)-f(x,y+k) = f_x(x_1,y+k)h
	\]
	\[
		f(x,y+k)-f(x,y) = f_y(x,y_1)k
	\]
	con $x_1\to x, y_1 \to y$ per $(h,k) \to (0,0)$
	otteniamo
	\[
		\frac{1}{\sqrt{h^2+k^2}}|f_x(x_1,y+k)-f_x(x,y)| |h| + \frac{1}{\sqrt{h^2+k^2}} |f_y(x,y_1) - f_y(x,y)| |k| =
	\]
	\[
		\frac{|h|}{\sqrt{h^2+k^2}}|f_x(x_1,y+k)-f_x(x,y)|+ \frac{|k|}{\sqrt{h^2+k^2}} |f_y(x,y_1) - f_y(x,y)|
	\]
	\[
		\frac{|h|}{\sqrt{h^2+k^2} } < 1 \qquad \frac{|k|}{\sqrt{h^2+k^2} } < 1
	\]
	segue
	\[
		\leqslant |f_x(x_1,y+k)-f_x(x,y)|+|f_y(x,y_1)-f_y(x,y)|
	\]
	Per la continuità delle derivate parziali si ha che l'espressione tende a $0$ per $(h,k)\to (0,0)$
\end{proof}
\subsection{Massimi e minimi}
\begin{enumerate}
	\item Punti di massimo e/o minimo di $f$ appartenenti ad un insieme aperto $\Omega$; si parla allora di calcolo degli estremi liberi ossia si vogliono trovare i punti di estremo interni al dominio
	\item Punti di massimo e/o minimo di $f$ appartenenti ad un insieme chiuso e limitato $K$ in cui considerare il fatto che possano appartenere alla frontiera
\end{enumerate}
Sia $A \subset \R^n$
\begin{proposition}[Punti critici]
	sia $f$ derivabile (dotata di derivate parziali prime) in $A$ (aperto) un punto $\vec{x}_0 \in A$ si dice stazionario o critico se $\grad f = \vec{0}$
\end{proposition}
\begin{definition}[Massimo e minimo locale]
	Sia $A \subseteq \R^n$ un aperto e $f:A \to \R$,un punto	$\vec{x}_0 \in A$ si dice minimo locale (massimo locale) per $f$ se esiste un intorno  $B_r(\vec{x}_0)$ tale che

	\[
		f(\vec{x}_0) \leqslant f(\vec{x}) \quad (f(\vec{x}_0) \geqslant f(\vec{x})) \qquad \forall \vec{x} \in A\cap B_r(\vec{x}_0)
	\]
\end{definition}
\begin{proposition}
	sia $f \in C^2$ e sia $\vec{x}_0 \in A$ un punto critico per $f$
	Se
	\begin{enumerate}
		\item $\mathrm{det} H_f(\vec{x}_0) > 0$
		      \begin{itemize}
			      \item $f_{xx} > 0$ allora $\vec{x}_0$ è un minimo locale
			      \item $f_{xx} < 0$ allora $\vec{x}_0$ è un massimo locale
		      \end{itemize}
		\item $\mathrm{det} H_f(\vec{x}_0) = 0$: caso dubbio
		\item $\mathrm{det} H_f(\vec{x}_0) < 0$: $\vec{x}_0$ è un punto di sella
	\end{enumerate}
\end{proposition}
Sia Q = \begin{pmatrix} a & b \\ b & c \end{pmatrix} una matrice simmetrica
\[
	|Q|= \mathrm{det } = ac-b^2
\]
Allora
\[
	|Q| > 0 \text{ e } a > 0 \implies Q > 0
\]
\[
	|Q| > 0 \text{ e } a < 0 \implies Q < 0
\]
$Q > 0 \iff ah_1^2 + 2bh_1 h_2 + ch_2^2 > 0 \quad \forall \vec{h}=(h_1,h_2) \neq \vec{0}$
\[
	\begin{gathered}
		ah_1^2+2bh_1 h_2 + c h_2^2 = a\left(h_1^2 + 2\frac{b}{a} h_1 h_2 \right) +ch_2^2 =\\
		a\left( h_1^2 + 2\frac{b}{a} h_1 h_2 + \frac{b^2}{a^2}h_2^2 - \frac{b^2}{a^2} h_2^2 \right) +ch_2^2= a\left(h_1+\frac{b}{a}h_2\right)^2 + \frac{ac-b^2}{a}h_2^2 =\\
		a\left(h_1+\frac{b}{a}h_2\right)^2 + \frac{|Q|}{a}h_2^2
	\end{gathered}
\]
\begin{definition}[Insieme chiuso]
	Un insieme $K$ è chiuso se l'insieme complementare in $\R^n$ è aperto
\end{definition}
\begin{definition}[Insieme Limitato]
	Un insieme $K$ si dice limitato se $\exists L\geq 0$ tale che $\|\vec{x}\| < L, \forall \vec{x} \in K$
\end{definition}
\begin{definition}[Massimo/Minimo globale]
	$f:K \to \R$, $\vec{x}_0 \in K$ se
	\[
		f(\vec{x}_0) \geqslant f(\vec{x}) \qquad (f(\vec{x}_0) \leqslant f(\vec{x})) \quad \forall \vec{x} \in \R^n
	\]
	$\vec{x}_0$ è il massimo (minimo) globale o assoluto
\end{definition}
\begin{theorem}[Teorema di Weierstrass]
	sia $f:K\subseteq \R^n \to \R$ una funzione continua in un insieme chiuso e limitato $K$ allora $f$ ammette massimo e minimo
\end{theorem}
\begin{definition}[Derivata direzionale]
	La derivata direzionale di una funzione scalare $f(\vec{x})=f\left(x_{1}, x_{2}, \ldots, x_{n}\right)$ lungo un vettore unitario $\vec{\lambda}=\left(\lambda_{1}, \ldots, \lambda_{n}\right)$ è definita dal limite:
	$$
		D_{\mathbf{\lambda}} f(\mathbf{x})=\lim _{h \rightarrow 0} \frac{f(\mathbf{x}+h \mathbf{\lambda})-f(\mathbf{x})}{h}
	$$
	In ogni punto $\mathbf{x}$, la derivata direzionale $D_{\mathrm{\lambda}} f(\mathbf{x})$ rappresenta la variazione di $f$ lungo $\mathbf{\lambda}$.
	Ad esempio, si consideri una funzione di due variabili $f: \Omega \rightarrow \mathbb{R}$, con $\Omega \subseteq \mathbb{R}^{2}$ un insieme aperto. Dato un vettore $\mathbf{\lambda}=\left(\lambda_{1}, \lambda_{2}\right)$, la derivata direzionale di $f$ lungo $\mathbf{\lambda}$, nel punto $\left(x_{0}, y_{0}\right) \in \Omega$, è data da:
	$$
		D_{\mathbf{\lambda}} f\left(x_{0}, y_{0}\right)=\lim _{h \rightarrow 0} \frac{f\left(x_{0}+h \lambda_{1}, y_{0}+h \lambda_{2}\right)-f\left(x_{0}, y_{0}\right)}{h}
	$$
	ed esiste se il limite è finito.
\end{definition}
\begin{theorem}
	Se $f$ è differenziabile in $\vec{x}\in A \subset \R^n$ allora $f$ ammette derivata direzionale in $\vec{x}$ rispetto ad ogni direzione $\vec{\lambda}$ e vale
	\[
		D_{\vec{\lambda}} f = \grad f \cdot \vec{\lambda}
	\]
\end{theorem}
\begin{definition}[funzione composta]
	sia $I\subset \R$ un intervallo, sia $A \subset \R^n$ un aperto, sia $\vec{x}:I \to \R^n$
	\[
		\vec{x}(t) = (x_1(t),\ldots,x_n(t)) \in A \quad \forall t \in I
	\]
	Sia $f:A \to \R$ una funzione, allora
	\[
		F(t) = f(\vec{x}(t))
	\]
\end{definition}
\begin{theorem}[Derivazione funzioni composte]
	Sia\\
	\begin{itemize}
		\item $\vec{x}(t) = (x_1(t),\ldots,x_n(t))$ derivabile
		\item $f$ differenziabile in $\vec{x}(t) \in A$
	\end{itemize}
	Allora $F$ risulta derivabile in $t \in I$
	\[
		F^{\prime}(t) = \grad f(\vec{x}(t))\cdot \vec{x}^{\prime}(t)
	\]
\end{theorem}
\begin{definition}[Insieme connesso]
	Un insieme aperto $A$ si dice connesso se non esistono aperti disgiunti non vuoti di $\R^n$ la cui unione sia $A$
\end{definition}
Se $\vec{x_0} \in A \subset \R^n$ è un punto stazionario ($\grad f(\vec{x_0}) = 0$)\\
La formula di taylor fornisce ($D^2f$ hessiano)
\[
	f(\vec{x}_0+\vec{h}) = f(\vec{x}_0) + \frac{1}{2} D^2 f(\vec{x}_0)\vec{h}\cdot \vec{h}+o(\|\vec{h}\|^2) \quad h\to 0
\]
Se $D^2 f(\vec{x}_0) \vec{h} \cdot \vec{h} > 0$ allora localmente
\[
	f(\vec{x}) \geqslant f(\vec{x}_0)
\]
Allora $\vec{x}_0$ è un punto di minimo locale
Analogamente Se $D^2 f(\vec{x}_0) \vec{h} \cdot \vec{h} < 0$ allora localmente
\[
	f(\vec{x}) \leqslant f(\vec{x}_0)
\]
$\vec{x}_0$ è un punto di massimo locale
\subsection{Serie di Taylor \R^n}
\begin{theorem}[Teorema di taylor (Resto di Lagrange)]
	Assumiamo $f \in C^2(A)$, $\vec{x}, \vec{x}+\vec{h} \in A$, $\vec{x}+t\vec{h} \in A$ con $t \in [0,1]$, $\vec{h}$ sufficientemente piccolo, $\exists \theta \in (0,1)$ tale che
	\[
		f(\vec{x}+\vec{h}) = f(\vec{x}) + \sum_{i=1}^{n} f_{x_i}(\vec{x})h_i + \frac{1}{2} \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x}+\theta \vec{h})h_i h_j
	\]
\end{theorem}
\begin{proof}
	Da $\vec{x}(t) = x+t\vec{h}$ con $\vec{h} \in \R^n$, $t \in [0, 1]$ e $h$ piccolo tale che $\vec{x}+t\vec{h} \in A$\\
	poniamo
	\[
		F(t) = f(\vec{x}+t\vec{h})
	\]
	Applichiamo la regola di derivazione per funzioni composte con $\vec{x}(t) = \vec{x} + t \vec{h}$, otteniamo
	\[
		F^{\prime}(t) = \sum_{i=1}^{n} f_{x_i}(\vec{x}+t\vec{h})h_i
	\]
	\[
		F^{\prime\prime}(t) = \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x}+t\vec{h})h_i h_j
	\]
	Applichiamo la formula di taylor per $n=1$
	\[
		F(x) = F(0)+F^{\prime}(0)+\frac{1}{2}F^{\prime\prime}(\theta)
	\]
	\[
		F(1) = F(0)+F^{\prime}(0)+\frac{1}{2}F^{\prime\prime}(\theta)
	\]
	Con $\theta \in (0,1)$
	Da $F(t) = f(\vec{x}+t\vec{h})$
	\[
		\begin{gathered}
			F(1) = f(\vec{x}+\vec{h}) \quad F(0) = f(\vec{x})\\
			F^{\prime}(0) = \sum_{i=1}^{n} f_{x_i}(x)h_i \qquad F^{\prime\prime}(t) = \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x}+t\vec{h})h_i h_j\\
			f(\vec{x}+\vec{h}) = f(\vec{x}) + \sum_{i=1}^{n} f_{x_i}(\vec{x})h_i + \frac{1}{2} \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x}+\theta \vec{h})h_i h_j
		\end{gathered}
	\]
\end{proof}
\begin{definition}[Norma di frobenius]
	$A \in K^{n\times m}$
	\[
		\|A\| = \sqrt{\sum_{i=1}^{n} \sum_{j=1}^{m} |a_{ij}|^2}
	\]
\end{definition}
\begin{proposition}
	\label{disuguaglianza norma}
	Sia $A \in \R^{n\times n}$ e $\vec{h} \in \R^n$, allora
	\[
		\|A \vec{h}\| \leqslant \|A\|\|\vec{h}\|
	\]
	Allora
	$$
		|A \vec{h} \cdot \vec{h}| \leq\|A \vec{h}\|\|\vec{h}\| \leq\|A\|\|\vec{h}\|^{2}
	$$
\end{proposition}
\begin{proof}
	\[
		\|A \vec{h}\| = \sqrt{\sum_{i=1}^{n} (a_{i1}h_1+a_{i 2}h_2+\ldots+ a_{in}h_n)^2}
	\]
	Ricordiamo la disuguaglianza di Cauchy-Schwarz
	$$
		\left(a_{i 1} h_{1}+a_{i 2} h_{2}+\ldots \ldots+a_{i n} h_{n}\right)^{2}=\left(\sum_{j=1}^{n} a_{i j} h_{j}\right)^{2} \leq\left(\sum_{j=1}^{n} h_{j}^{2}\right)\left(\sum_{j=1}^{n} a_{i j}^{2}\right)
	$$
	\[
		\|A \vec{h}\| = \sqrt{\sum_{i=1}^{n} (a_{i1}h_1+a_{i 2}h_2+\ldots+ a_{in}h_n)^2} \leqslant \sqrt{\sum_{i=1}^{n} \sum_{j=1}^{n} a_{ij}^2}\|\vec{h}\|=\|A\|\|\vec{h}\|
	\]
\end{proof}
\begin{theorem}[Formuala di Taylor (Resto di Peano)]
	Assumiamo $f \in C^2(A)$, $\vec{x}, \vec{x}+\vec{h} \in A, \vec{x}+t\vec{h} \in A$ con $t \in [0,1]$, $\vec{h}$ sufficientemente piccolo
	\[
		f(\vec{x}+\vec{h}) = f(\vec{x}) + \sum_{i=1}^{n} f_{x_i} (\vec{x})h_i + \frac{1}{2} \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x})h_i h_j + o(\|\vec{h}\|^2) \quad \vec{h}\to 0
	\]
\end{theorem}
\begin{proof}
	Dobbiamo dimostrare che
	\[
		\sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x}+\theta \vec{h})h_i h_j = \sum_{i,j=1}^{n} f_{x_i x_j}(\vec{x})h_i h_j + o(\|\vec{h}\|^2) \quad \vec{h} \to 0
	\]
	\[
		\sum_{i,j=1}^{n} (f_{x_i x_j}(\vec{x}+\theta \vec{h}) - f_{x_i x_j}(\vec{x}))h_i h_j = o(\|\vec{h}\|^2) \quad \vec{h} \to 0
	\]
	Grazie alla \ref{disuguaglianza norma} ponendo $A = D^2 f(\vec{x} + \theta \vec{h}) - D^2 f(\vec{x})$
	\[
		\frac{|\sum_{i,j=1}^{n} (f_{x_i x_j}(\vec{x}+\theta \vec{h}) - f_{x_i x_j}(\vec{x}))h_i h_j|}{\|\vec{h}\|^2} \leqslant \| D^2 f(\vec{x}+\theta\vec{h})-D^2f(\vec{x}) \|
	\]
	Da $f \in C^2$ si ottiene
	\[
		\| D^2 f(\vec{x}+\theta\vec{h})-D^2f(\vec{x}) \| = 0 \quad \text{ per } \vec{h} \to 0
	\]
\end{proof}
